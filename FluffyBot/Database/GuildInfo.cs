﻿using System.ComponentModel.DataAnnotations;

namespace FluffyBot.Database {
	public class GuildInfo {

		[Key, Required]
		public ulong GuildID {
			get;
			set;
		}

		public ulong? TransactionChannelID {
			get;
			set;
		} = null;

		public ulong? LobbyID {
			get;
			set;
		} = null;

	}
}
