﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FluffyBot.Database {

	/// <summary>
	/// Represents a user on the Discord server and on the database.
	/// </summary>
	public partial class User {

		/// <summary>
		/// Holds the Discord ID of the user to link it back to them.
		/// </summary>
		[Key, Column( "DiscordID" )]
		public ulong DiscordId {
			get;
			set;
		}

		/// <summary>
		/// Suppose to be a boolean.
		/// Please use <see cref="GetAdultAsBool()"/> and <see cref="SetAdultAsBool(bool)"/>.
		/// </summary>
		[Column( "Adult" )]
		public sbyte Adult {
			get => ( sbyte )( adult ? 1 : 0 ); // Returns a converted bool to a signed byte.

			set => adult = value == 0 ? false : true; // Converts a passed signed byte to either true or false for the private bool.
		}

		/// <summary>
		/// How many points the user has.
		/// </summary>
		public int Points {
			get;
			set;
		}

		/// <summary>
		/// The last time the user has been warned.
		/// </summary>
		[Column]
		public DateTime? LastWarning {
			get;
			set;
		}

		/// <summary>
		/// The reason for the last warning.
		/// </summary>
		[Column]
		public string Reason {
			get;
			set;
		}

		private bool adult; // A private bool that holds rather if the user is allowed in NSFW channels.

		/// <summary>
		/// Used to get if the user is allowed to see adult content.
		/// </summary>
		/// <returns><see langword="true"/> if the user is able to see adult content, otherwise <see langword="false"/>.</returns>
		public bool GetAdultAsBool() => adult;

		/// <summary>
		/// Set's if the user is able to see if they are able to see NSFW content.
		/// </summary>
		/// <param name="adult">Reather the user is able to see adult content or not.</param>
		public void SetAdultAsBool( bool adult ) => this.adult = adult;

		/// <summary>
		/// Prints out the values of the user.
		/// </summary>
		/// <returns></returns>
		public override string ToString() => $"DiscordID: {DiscordId}\nAdult: {adult}\nLast Warning: {LastWarning?.ToString()}\nReason: {Reason}";
	}
}