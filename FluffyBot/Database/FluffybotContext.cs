﻿using System.IO;
using System.Runtime.InteropServices;

using FluffyBot.Utilities;

using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;

namespace FluffyBot.Database {
	public partial class FluffyBotContext : DbContext {

		public static string DatabaseFile {
			get;
		}

		private static bool exists = false;

		static FluffyBotContext() {
			if ( RuntimeInformation.IsOSPlatform( OSPlatform.Windows ) ) {
				DatabaseFile = @"C:\ProgramData\FluffyBot\Database";
			} else if ( RuntimeInformation.IsOSPlatform( OSPlatform.OSX ) ) {
				DatabaseFile = "/Library/Application Support/FluffyBot/Database";
			} else if ( RuntimeInformation.IsOSPlatform( OSPlatform.Linux ) ) {
				DatabaseFile = "/etc/fluffybot/database";
			} else {
				DatabaseFile = ".";
			}

			DatabaseFile = Path.Combine( DatabaseFile, "core.db" );
		}

		public virtual DbSet<User> User {
			get;
			set;
		}

		public virtual DbSet<Image> Image {
			get;
			set;
		}

		public virtual DbSet<GuildInfo> Guilds {
			get;
			set;
		}

		public FluffyBotContext() {
			if ( exists ) {
				return;
			}

			if ( !File.Exists( DatabaseFile ) ) {
				if ( !Directory.Exists( Path.GetDirectoryName( DatabaseFile ) ) ) {
					_ = Directory.CreateDirectory( Path.GetDirectoryName( DatabaseFile ) );
				}

				Database.EnsureCreated();
				exists = true;
			} else {
				var dbFile = new FileInfo( DatabaseFile );

				if ( dbFile.Length > 0 ) {
					exists = true;
					return;
				}

				Database.EnsureDeleted();
				Database.EnsureCreated();
			}
		}

		public FluffyBotContext( DbContextOptions<FluffyBotContext> options ) : base( options ) {
		}

		protected override void OnConfiguring( DbContextOptionsBuilder optionsBuilder ) {
			var settings = SettingManager.Singleton;

			if ( !optionsBuilder.IsConfigured ) {
				var builder = new SqliteConnectionStringBuilder {
					Cache = SqliteCacheMode.Private,
					Mode = SqliteOpenMode.ReadWriteCreate,
					DataSource = DatabaseFile
				};
				var opts = builder.ToString();

				optionsBuilder.UseSqlite( opts );
			}
		}
	}
}
