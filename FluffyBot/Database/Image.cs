﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FluffyBot.Database {
	public partial class Image {

		[Required]
		public ulong GuildID {
			get;
			set;
		}

		[Required]
		public string Url {
			get;
			set;
		}

		[Required]
		public string Name {
			get;
			set;
		}

		[Required]
		public Guid Id {
			get;
			set;
		}
	}
}
