﻿using System;

namespace FluffyBot.Attributes {

	[AttributeUsage( AttributeTargets.Method, Inherited = false, AllowMultiple = false )]
	public sealed class HandleLinkInMessageAttribute : Attribute {
		public string[] Links { get; }

		public HandleLinkInMessageAttribute( params string[] links ) {
			Links = links;
		}
	}
}
