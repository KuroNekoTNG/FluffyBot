﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Discord;
using Discord.Commands;
using Discord.WebSocket;

using FluffyBot.Attributes;
using FluffyBot.Database;
using FluffyBot.Utilities;
using FluffyBot.Utilities.Extension;
using FluffyBot.Utilities.Logging;

using Microsoft.Extensions.DependencyInjection;

using LogMessage = Discord.LogMessage;
using Module = FluffyBot.Utilities.Extension.Module;

namespace FluffyBot.Core {
	public sealed class DiscordController {

		public delegate Task ReactionEvent( Cacheable<IUserMessage, ulong> cacheMessage, ISocketMessageChannel channel, SocketReaction reaction, bool removed );

		public delegate Task UserJoinedEvent( SocketGuildUser user );

		public delegate Task UserLeftEvent( SocketGuildUser user );

		public delegate Task UserBannedEvent( SocketUser user, SocketGuild guild );

		public delegate Task UserUnbannedEvent( SocketUser user, SocketGuild guild );

		/// <summary>
		/// Used to send out that the bot has recieved a message from Discord.
		/// </summary>
		/// <remarks>
		/// Returning <see langword="true"/> will make the bot move on to the next method.
		/// However, returning <see langword="false"/> will tell the bot that the message was deleted/told to be deleted.
		/// </remarks>
		/// <param name="message">THe message from Discord.</param>
		/// <returns><see langword="true"/> if the message was not deleted, otherwise <see langword="false"/>.</returns>
		public delegate Task<bool> MessageRecievedEvent( SocketMessage message );

		public static DiscordController Controller {
			get;
		} = new DiscordController();

		public ReactionEvent Reaction;

		public UserJoinedEvent UserJoined;

		public UserLeftEvent UserLeft;

		public UserBannedEvent UserBanned;

		public UserUnbannedEvent UserUnbanned;

		public MessageRecievedEvent MessageRecieved;

		public DiscordSocketClient Client {
			get;
		}

		private Dictionary<string, MethodInfo> linkMethodProviders;

		private CommandService comService;
		private IServiceProvider serviceProvider;

		private DiscordController() {
			linkMethodProviders = new Dictionary<string, MethodInfo>();

			Client = new DiscordSocketClient( new DiscordSocketConfig() {
				DefaultRetryMode = RetryMode.AlwaysRetry,
				LogLevel = Program.DebugBuild ? LogSeverity.Verbose : LogSeverity.Info
			} );

			comService = new CommandService( new CommandServiceConfig() {
				CaseSensitiveCommands = true,
				SeparatorChar = ';',
				LogLevel = Program.DebugBuild ? LogSeverity.Verbose : LogSeverity.Info
			} );

			serviceProvider = new ServiceCollection()
				.AddSingleton( Client )
				.AddSingleton( comService )
				.BuildServiceProvider();

			Client.Log += LogClient;
			Client.UserJoined += UserJoinedGuild;
			Client.UserLeft += UserLeftGuild;
			Client.UserBanned += UserBannedFromGuild;
			Client.UserUnbanned += UserUnbannedFromGuild;
			Client.MessageReceived += Reciever;
			Client.ReactionAdded += ReactionAdded;
			Client.ReactionRemoved += ReactionRemoved;
			Client.GuildAvailable += GuildAvailable;

			// This is here to get the SettingManager to start doing its thing.
			_ = SettingManager.Singleton.GetType();

			ModuleHandler.NewModuleLoaded += NewModuleLoaded;
		}

		private Task GuildAvailable( SocketGuild guild ) {
			using ( var dbContext = new FluffyBotContext() ) {
				foreach ( var user in guild.Users ) {
					var found = false;

					if ( user.IsBot ) {
						continue;
					}

					LogHandler.Log( $"Checking to see if `{user.Username}` from `{guild.Name}` is in the database." );

					foreach ( var dbUser in dbContext.User ) {
						if ( dbUser.DiscordId == user.Id ) {
							found = true;

							break;
						}
					}

					if ( !found ) {
						LogHandler.Log( $"The user is not, adding them to the database." );

						dbContext.User.Add( new User() {
							DiscordId = user.Id
						} );
					}
				}
			}

			return Task.CompletedTask;
		}

		~DiscordController() => Client.Dispose();

		public Task InitiateBotConnection() {
			ModuleHandler.LoadModules();

			var token = File.ReadAllText( Path.Combine( SettingManager.SETTINGS_FOLDER, "token" ) ).Trim();

			Client.LoginAsync( TokenType.Bot, token ).Wait();

			return Client.StartAsync();
		}

		private void NewModuleLoaded( Assembly asm, Module module ) {
			comService.AddModulesAsync( asm, serviceProvider ).Wait();

			var types = asm.GetTypes();

			for ( var i = 0; i < types.Length; ++i ) {
				var methods = types[i].GetMethods()
					.Where( m => m.IsStatic && m.GetCustomAttributes<HandleLinkInMessageAttribute>().Count() > 0 );

				foreach ( var method in methods ) {
					var linkAttr = method.GetCustomAttributes<HandleLinkInMessageAttribute>();

					foreach ( var attr in linkAttr ) {
						for ( var j = 0; j < attr.Links.Length; ++j ) {
							LogHandler.Log( $"Method `{method.Name}` from `{method.DeclaringType.FullName}` will handle links for `{attr.Links[j]}`." );

							linkMethodProviders.Add( attr.Links[j], method );
						}
					}
				}
			}
		}

		#region Forward Event Call Functions
		private Task ReactionRemoved( Cacheable<IUserMessage, ulong> cachedMessage, ISocketMessageChannel messageChannel, SocketReaction reaction ) {
			try {
				return Reaction?.Invoke( cachedMessage, messageChannel, reaction, true );
			} catch ( Exception e ) {
				LogHandler.LogError( $"Error occured while invoking methods.", e );
				return Task.CompletedTask;
			}
		}

		private Task ReactionAdded( Cacheable<IUserMessage, ulong> cachedMessage, ISocketMessageChannel messageChannel, SocketReaction reaction ) {
			try {
				return Reaction?.Invoke( cachedMessage, messageChannel, reaction, false );
			} catch ( Exception e ) {
				LogHandler.LogError( "Error occured while invoking methods.", e );
				return Task.CompletedTask;
			}
		}

		private Task UserUnbannedFromGuild( SocketUser user, SocketGuild guild ) {
			try {
				return UserUnbanned?.Invoke( user, guild );
			} catch ( Exception e ) {
				LogHandler.LogError( "Error occured while invoking methods.", e );
				return Task.CompletedTask;
			}
		}

		private Task UserBannedFromGuild( SocketUser user, SocketGuild guild ) {
			try {
				return UserBanned?.Invoke( user, guild );
			} catch ( Exception e ) {
				LogHandler.LogError( "Error occured while invoking methods.", e );
				return Task.CompletedTask;
			}
		}

		private Task UserLeftGuild( SocketGuildUser guildUser ) {
			try {
				return UserLeft?.Invoke( guildUser );
			} catch ( Exception e ) {
				LogHandler.LogError( "Error occured while invoking methods.", e );
				return Task.CompletedTask;
			}
		}

		private Task UserJoinedGuild( SocketGuildUser guildUser ) {
			try {
				return UserJoined?.Invoke( guildUser );
			} catch ( Exception e ) {
				LogHandler.LogError( "Error occured while invoking methods.", e );
				return Task.CompletedTask;
			}
		}
		#endregion

		private Task LogClient( LogMessage message ) {
			switch ( message.Severity ) {
				case LogSeverity.Info:
				case LogSeverity.Verbose:
				case LogSeverity.Debug:
					LogHandler.Log( message.Message );
					break;
				case LogSeverity.Error:
					LogHandler.LogError( message.Message, message.Exception );
					break;
				default:
				case LogSeverity.Warning:
					LogHandler.LogWarning( message.Message );
					break;
				case LogSeverity.Critical:
					LogHandler.LogExcption( message.Message, message.Exception );
					break;
			}

			return Task.CompletedTask;
		}

		private async Task Reciever( SocketMessage msg ) {
			var argPos = 0;
			var methods = MessageRecieved?.GetInvocationList();
			SocketCommandContext context;
			IResult result;

			for ( var i = 0; methods is Delegate[] && i < methods.Length; ++i ) {
				var declareType = default( object );
				var method = methods[i].GetMethodInfo();

				if ( !method.IsStatic ) {
					var constructor = method.DeclaringType.GetConstructors().FirstOrDefault( c => c.GetParameters().Length == 0 );

					if ( constructor is null ) {
						continue;
					}

					declareType = constructor.Invoke( null );
				}

				var task = methods[i].GetMethodInfo().Invoke( declareType, new[] { msg } ) as Task<bool>;
				var asyncResult = task.GetAwaiter().GetResult();

				if ( !asyncResult ) {
					return;
				}
			}

			if ( !( msg is SocketUserMessage message ) || message.Author.IsBot ) {
				return;
			}

			if ( MessageContainsLink( message, out var linkMethods ) ) {
				new Task( () => {
					foreach ( (var link, var method) in linkMethods ) {
						try {
							method.Invoke( null, new object[] { message, link } );
						} catch ( Exception e ) {
							LogHandler.LogError( $"Something happened while invoking `{method.Name}` in `{method.DeclaringType.FullName}`.", e );
						}
					}
				} ).Start();
			}

			if ( message.HasStringPrefix( "f!", ref argPos ) ) {
				context = new SocketCommandContext( Client, message );
				result = await comService.ExecuteAsync( context, argPos, serviceProvider );

				if ( !result.IsSuccess ) {
					switch ( result.Error ) {
						case CommandError.BadArgCount:
							await message.Channel
								.SendMessageAsync( $"{message.Author.Mention}, you have provided the wrong amount of arguments for the command." );
							break;
						case CommandError.Exception:
							LogHandler.LogExcption( $"Problem occured mid execution of command.\n{result.ErrorReason}", null );
							await message.Channel
								.SendMessageAsync( $"{message.Author.Mention}, sorry, unable to satify your request. The following problem occured:\n{result.ErrorReason}" );
							break;
						case CommandError.MultipleMatches:
							LogHandler.LogError( result.ErrorReason, null );
							await message.Channel
								.SendMessageAsync( $"{message.Author.Mention}, sorry, I don't know which command to do as I know of multiple that are a match." );
							break;
						case CommandError.ParseFailed:
							await message.Channel
								.SendMessageAsync( $"{message.Author.Mention}, I have failed at understanding your input." );
							break;
						case CommandError.UnknownCommand:
							await message.Channel
								.SendMessageAsync( $"{message.Author.Mention}, I don't understand that command." );
							break;
						case CommandError.UnmetPrecondition:
							await message.Channel
								.SendMessageAsync( $"{message.Author.Mention}, I am sorry, but a precondition has failed. Please contact the mods/bot owner about the command." );
							break;
						case CommandError.Unsuccessful:
						case CommandError.ObjectNotFound:
							await message.Channel
								.SendMessageAsync( $"{message.Author.Mention}\n__**SOMETHING HAPPENED**__\nSomething happened" );
							break;
					}
				}
			}

			bool MessageContainsLink( SocketUserMessage message, out IEnumerable<(string link, MethodInfo method)> methods ) {
				var linkMethods = new LinkedList<(string link, MethodInfo method)>();
				var matches = Regex.Matches( message.Content, @"\s?(?:https?:\/{2})[a-zA-Z0-9]+(?:\.[a-zA-Z0-9]+)\/*\S*\s?" ) as IEnumerable<Match>;

				foreach ( var pair in linkMethodProviders ) {
					foreach ( var match in matches ) {
						if ( !match.Value.Contains( pair.Key ) ) {
							continue;
						}

						linkMethods.AddLast( (match.Value, pair.Value) );
					}
				}

				methods = linkMethods;

				return linkMethods.Count > 0;
			}
		}
	}
}