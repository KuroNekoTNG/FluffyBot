﻿using Discord.Commands;

using FluffyBot.Utilities.Extension;

namespace FluffyBot.Core {
	public abstract class CommandCore : ModuleBase<SocketCommandContext> {

		protected Module module;

		protected CommandCore() {
			var myType = GetType();
			var moduleType = ModuleHandler.ExtractModuleFromType( GetType() );

			module = ModuleHandler.GetMatchingModule( moduleType );
		}

	}
}
