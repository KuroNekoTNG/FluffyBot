﻿using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

using FluffyBot.Core;
using FluffyBot.Utilities.Logging;

namespace FluffyBot {
	internal class Program {

		/// <summary>
		/// This is the key to access the token that the bot will use to connect to Discord.
		/// </summary>
		public const string TOKEN_KEY = "token";

		/// <summary>
		/// Used to tell if this is a debug build of the project or not.
		/// </summary>
		public static bool DebugBuild =>
#if DEBUG
				true;
#else
				false;
#endif

		[DllImport( "libc" )]
		private static extern int getuid();
		[DllImport( "libc" )]
		private static extern int setuid( uint uid );
		[DllImport( "libc" )]
		private static extern int setgid( uint gid );
		[DllImport( "libc", EntryPoint = "chmod" )]
		private static extern int sys_chmod( [MarshalAs( UnmanagedType.LPStr )] string path, uint mode );
		[DllImport( "libc" )]
		private static extern int chown( [MarshalAs( UnmanagedType.LPStr )]string path, uint owner, uint group );

		public static Task Main( string[] args ) {
			if ( !DebugBuild && RuntimeInformation.IsOSPlatform( OSPlatform.Windows ) ) {
				throw new Exception( "Please run this through Services." );
			}

			Console.WriteLine( "Before new code." );

			ChangeOwnsership();

			LoadAllAssemblies();

			if ( ( RuntimeInformation.IsOSPlatform( OSPlatform.Linux ) || RuntimeInformation.IsOSPlatform( OSPlatform.FreeBSD ) ) && getuid() == 0 ) {
				if ( setuid( 666 ) == -1 /*|| setgid( 666 ) == -1*/ ) {
					Console.WriteLine( "Failed to drop down." );

					Environment.ExitCode = -1;

					return Task.CompletedTask;
				}
			}

			Console.WriteLine( "After new code." );

			try {
				return CommonStart( args );
			} catch ( Exception e ) {
				Console.WriteLine( $"Exception:\t{e.GetType().FullName}\nMessage:\t{e.Message}\nStacktrace:\n{e.StackTrace}" );

				var inner = e.InnerException;

				while ( inner is Exception ) {
					Console.WriteLine( $"INNER EXCEPTION\nType:\t{inner.GetType().FullName}\nMessage:\t{inner.Message}\nStacktrace:\n{inner.StackTrace}" );

					inner = inner.InnerException;
				}

				return Task.CompletedTask;
			}
		}

		[Conditional( "NIX_SINGLE_FILE" )]
		private static void LoadAllAssemblies() {
			var asms = Directory.GetFiles( Path.GetDirectoryName( Assembly.GetExecutingAssembly().Location ), "*.dll", SearchOption.AllDirectories );
			var asmInAppDomain = AppDomain.CurrentDomain.GetAssemblies();

			for ( int i = 0; i < asmInAppDomain.Length; ++i ) {
				Console.WriteLine( $"{asmInAppDomain[i].Location}" );
			}

			for ( int i = 0; i < asms.Length; ++i ) {
				var inAppDomain = false;

				for ( int j = 0; j < asmInAppDomain.Length; ++j ) {
					if ( AssemblyName.GetAssemblyName( asms[i] ).Name == asmInAppDomain[j].GetName().Name ) {
						inAppDomain = true;
						break;
					}
				}

				if ( inAppDomain ) {
					continue;
				}

				_ = Assembly.LoadFile( asms[i] );
			}
		}

		[SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "There for future expansion." )]
		internal static Task CommonStart( string[] args ) {
			var controller = DiscordController.Controller;

			controller.InitiateBotConnection().Wait();

			return Task.Delay( -1 );
		}

		[Conditional( "NIX_SINGLE_FILE" )]
		private static void ChangeOwnsership() {
			foreach ( var file in Directory.GetFiles( Path.GetDirectoryName( Assembly.GetExecutingAssembly().Location ), "*", SearchOption.AllDirectories ) ) {
				sys_chmod( file, 510 );
				chown( file, 666, 666 );
			}
		}
	}
}
