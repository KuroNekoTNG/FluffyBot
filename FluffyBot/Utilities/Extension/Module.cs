﻿namespace FluffyBot.Utilities.Extension {
	public abstract class Module {

		protected static SettingManager Settings => SettingManager.Singleton;

		protected Module() {

		}

		public bool TryAddSetting( string key, object value ) => Settings.TryAddSetting( this, key, value );

		public bool TrySetSetting( string key, object value ) => Settings.TrySetSetting( this, key, value );

		public bool TryGetSetting( string key, out object value ) => Settings.TryGetSetting( this, key, out value );

		public bool TryGetSetting<T>( string key, out T value ) => Settings.TryGetSetting( this, key, out value );

	}
}