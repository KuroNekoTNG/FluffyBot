﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

using FluffyBot.Utilities.Logging;

namespace FluffyBot.Utilities.Extension {
	public static class ModuleHandler {

		public delegate void NewModuleLoadedEvent( Assembly asm, Module module );

		public static readonly string MODULE_FOLDER;

		public static bool ModulesLoaded => modules.Count > 0;

		public static NewModuleLoadedEvent NewModuleLoaded;

		private static LinkedList<Module> modules = new LinkedList<Module>();

		static ModuleHandler() {
			switch ( Environment.OSVersion.Platform ) {
				case PlatformID.Win32NT:
					MODULE_FOLDER = @"C:\ProgramData\FluffyBot\Modules";
					break;
				case PlatformID.Unix:
					MODULE_FOLDER = "/var/lib/fluffybot";
					break;
				case PlatformID.Win32S:
				case PlatformID.Win32Windows:
				case PlatformID.WinCE:
				case PlatformID.Xbox:
				case PlatformID.MacOSX:
					throw new Exception( "The bot will/should not be running on this platform." );
				default:
					MODULE_FOLDER = "./modules";
					break;
			}
		}

		public static Type ExtractModuleFromType( Type type ) {
			var moduleType = typeof( Module );
			if ( moduleType.IsAssignableFrom( type ) ) {
				return type;
			}

			return type.Assembly.GetTypes().FirstOrDefault( t => typeof( Module ).IsAssignableFrom( t ) );
		}

		public static Module GetMatchingModule( Type type ) {
			type = ExtractModuleFromType( type );

			if ( type is null ) {
				throw new Exception( "Unable to locate module." );
			}

			foreach ( var module in modules ) {
				if ( module.GetType() == type ) {
					return module;
				}
			}

			return null;
		}

		public static void LoadModules() {
			if ( modules.Count > 0 ) {
				throw new Exception( $"{modules.Count} modules have already been loaded." );
			}

			foreach ( var moduleFolder in Directory.EnumerateDirectories( MODULE_FOLDER, "*", SearchOption.TopDirectoryOnly ) ) {
				LogHandler.Log( $"Looking for modules in `{moduleFolder}`." );

				try {
					LoadModule( moduleFolder );
				} catch ( Exception e ) {
					LogHandler.LogError( $"Something happened while attempting to load module folder `{moduleFolder}`.", e );
				}
			}
		}

		private static void LoadModule( string path ) {
			foreach ( var asmFile in Directory.EnumerateFiles( path, "*.dll", SearchOption.TopDirectoryOnly ) ) {
				var asm = Assembly.LoadFile( asmFile );

				FindDependencies( asm );

				var moduleTypes = asm.GetTypes()
					.Where( t => typeof( Module ).IsAssignableFrom( t ) )
					.ToList();

				for ( var i = 0; i < moduleTypes.Count; ++i ) {
					var moduleType = moduleTypes[i];
					var moduleCons = moduleType.GetConstructors()
						.FirstOrDefault( c => c.GetParameters().Length == 0 );

					if ( moduleCons == default ) {
						LogHandler.LogError( $"Unknown module state, claims to be module yet does not have proper constructor.", null );
						continue;
					}

					var module = ( Module )moduleCons.Invoke( null );

					modules.AddLast( module );

					NewModuleLoaded?.Invoke( asm, module );
				}
			}

			static void FindDependencies( Assembly asm ) {
				var deps = asm.GetReferencedAssemblies();
				var loadedAsms = AppDomain.CurrentDomain.GetAssemblies();
				var needToLoad = new List<AssemblyName>();

				for ( int i = 0; i < deps.Length; ++i ) {
					var asmName = deps[i];
					var loaded = false;

					LogHandler.Log( $"Verifying `{asmName.Name}` has been loaded." );

					for ( int j = 0; j < loadedAsms.Length; ++j ) {
						if ( deps[j].Name == loadedAsms[j].GetName().Name ) {
							loaded = true;

							break;
						}
					}

					if ( loaded ) {
						LogHandler.Log( "Dependency has been loaded." );

						continue;
					}

					LogHandler.Log( "Adding to list of depencencies to load." );

					needToLoad.Add( asmName );
				}

				foreach ( var asmFile in Directory.GetFiles( Path.GetDirectoryName( asm.Location ), "*.dll", SearchOption.AllDirectories ) ) {
					if ( asmFile == asm.Location ) {
						continue;
					}

					for ( int i = 0; i < needToLoad.Count; ++i ) {
						if ( needToLoad[i].Name == AssemblyName.GetAssemblyName( asmFile ).Name ) {
							LogHandler.Log( $"Loading dependency from `{asmFile}`." );

							_ = Assembly.LoadFile( asmFile );

							--i;
						}
					}
				}
			}
		}

	}
}