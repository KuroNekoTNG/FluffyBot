﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Reflection;
using System.Text;

using Newtonsoft.Json;

using Module = FluffyBot.Utilities.Extension.Module;
using ModuleHandler = FluffyBot.Utilities.Extension.ModuleHandler;

namespace FluffyBot.Utilities {

	/// <summary>
	/// As the name suggests, this class is focused on providing settings to all the modules that the bots load.
	/// </summary>
	public sealed class SettingManager {

		/// <summary>
		/// Where the folder for settings are located.
		/// </summary>
		public static readonly string SETTINGS_FOLDER;

		/// <summary>
		/// Holds the reference to the single instance of the class.
		/// </summary>
		public static SettingManager Singleton {
			get;
		} = new SettingManager();

		/// <summary>
		/// How the settings JSON is stored.
		/// </summary>
		public static readonly JsonSerializerSettings JSON_SETTINGS = new JsonSerializerSettings() {
			CheckAdditionalContent = true,
			ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor,
			DateFormatHandling = DateFormatHandling.IsoDateFormat,
			DateParseHandling = DateParseHandling.DateTime,
			DateTimeZoneHandling = DateTimeZoneHandling.Utc,
			DefaultValueHandling = DefaultValueHandling.Include,
			FloatFormatHandling = FloatFormatHandling.Symbol,
			FloatParseHandling = FloatParseHandling.Double,
			Formatting = Formatting.Indented,
			MetadataPropertyHandling = MetadataPropertyHandling.ReadAhead,
			MissingMemberHandling = MissingMemberHandling.Ignore,
			NullValueHandling = NullValueHandling.Include,
			ObjectCreationHandling = ObjectCreationHandling.Auto,
			PreserveReferencesHandling = PreserveReferencesHandling.All,
			ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
			StringEscapeHandling = StringEscapeHandling.EscapeHtml,
			TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Full,
			TypeNameHandling = TypeNameHandling.All
		};

		#region Path Generation Methods
		/// <summary>
		/// Creates the path that points to a settings.json file based on the <see cref="Module"/> passed to it.
		/// </summary>
		/// <seealso cref="SettingFileLocationFor(Type)"/>
		/// <seealso cref="SettingFileLocationFor(Assembly)"/>
		/// <seealso cref="SettingFileLocationFor(AssemblyName)"/>
		/// <seealso cref="SettingFolderLocationFor(Module)"/>
		/// <seealso cref="SettingFolderLocationFor(Type)"/>
		/// <seealso cref="SettingFolderLocationFor(Assembly)"/>
		/// <seealso cref="SettingFolderLocationFor(AssemblyName)"/>
		/// <param name="module">The <see cref="Module"/> of the bot module.</param>
		/// <returns>A string that points to the settings file.</returns>
		public static string SettingFileLocationFor( Module module ) => SettingFileLocationFor( module.GetType() );

		/// <summary>
		/// Creates the path that points to a settings.json file based on the mod.
		/// </summary>
		/// <seealso cref="SettingFileLocationFor(Module)"/>
		/// <seealso cref="SettingFileLocationFor(Assembly)"/>
		/// <seealso cref="SettingFileLocationFor(AssemblyName)"/>
		/// <seealso cref="SettingFolderLocationFor(Module)"/>
		/// <seealso cref="SettingFolderLocationFor(Type)"/>
		/// <seealso cref="SettingFolderLocationFor(Assembly)"/>
		/// <seealso cref="SettingFolderLocationFor(AssemblyName)"/>
		/// <param name="type">Any type from the assembly that is calling.</param>
		/// <returns>A string that points to the settings file.</returns>
		public static string SettingFileLocationFor( Type type ) => SettingFileLocationFor( type.Assembly );

		/// <summary>
		/// Creates a path that points to the settings.json file based off the assembly.
		/// </summary>
		/// <seealso cref="SettingFileLocationFor(Module)"/>
		/// <seealso cref="SettingFileLocationFor(Type)"/>
		/// <seealso cref="SettingFileLocationFor(AssemblyName)"/>
		/// <seealso cref="SettingFolderLocationFor(Module)"/>
		/// <seealso cref="SettingFolderLocationFor(Type)"/>
		/// <seealso cref="SettingFolderLocationFor(Assembly)"/>
		/// <seealso cref="SettingFolderLocationFor(AssemblyName)"/>
		/// <param name="asm">The <see cref="Assembly"/> of the caller.</param>
		/// <returns>A string that points to the settings file.</returns>
		public static string SettingFileLocationFor( Assembly asm ) => SettingFileLocationFor( asm.GetName() );

		/// <summary>
		/// Creates a path that points to the settings.json file based off the assembly name.
		/// </summary>
		/// <seealso cref="SettingFileLocationFor(Module)"/>
		/// <seealso cref="SettingFileLocationFor(Type)"/>
		/// <seealso cref="SettingFileLocationFor(Assembly)"/>
		/// <seealso cref="SettingFolderLocationFor(Module)"/>
		/// <seealso cref="SettingFolderLocationFor(Type)"/>
		/// <seealso cref="SettingFolderLocationFor(Assembly)"/>
		/// <seealso cref="SettingFolderLocationFor(AssemblyName)"/>
		/// <param name="asmName">The <see cref="AssemblyName"/> of the caller's <see cref="Assembly"/>.</param>
		/// <returns>A string that points to the settings file.</returns>
		public static string SettingFileLocationFor( AssemblyName asmName ) => SettingFileLocationFor( asmName.Name );

		private static string SettingFileLocationFor( string asmName ) => Path.Combine( SettingFolderLocationFor( asmName ), "settings.json" );

		/// <summary>
		/// Creates a path that points to the folder location of the settings.
		/// </summary>
		/// <seealso cref="SettingFileLocationFor(Module)"/>
		/// <seealso cref="SettingFileLocationFor(Type)"/>
		/// <seealso cref="SettingFileLocationFor(Assembly)"/>
		/// <seealso cref="SettingFileLocationFor(AssemblyName)"/>
		/// <seealso cref="SettingFolderLocationFor(Type)"/>
		/// <seealso cref="SettingFolderLocationFor(Assembly)"/>
		/// <seealso cref="SettingFolderLocationFor(AssemblyName)"/>
		/// <param name="module">The <see cref="Module"/> that acts as the identifier for the module.</param>
		/// <returns>A string that points to the folder containing the settings file.</returns>
		public static string SettingFolderLocationFor( Module module ) => SettingFolderLocationFor( module.GetType() );

		/// <summary>
		/// Creates a path that points to the folder containing the settings file.
		/// </summary>
		/// <seealso cref="SettingFileLocationFor(Module)"/>
		/// <seealso cref="SettingFileLocationFor(Type)"/>
		/// <seealso cref="SettingFileLocationFor(Assembly)"/>
		/// <seealso cref="SettingFileLocationFor(AssemblyName)"/>
		/// <seealso cref="SettingFolderLocationFor(Module)"/>
		/// <seealso cref="SettingFolderLocationFor(Assembly)"/>
		/// <seealso cref="SettingFolderLocationFor(AssemblyName)"/>
		/// <param name="type">The <see cref="Type"/> of the caller.</param>
		/// <returns>A string that points to the folder containing the settings file.</returns>
		public static string SettingFolderLocationFor( Type type ) => SettingFolderLocationFor( type.Assembly );

		/// <summary>
		/// Creates a path that points to the folder containing the settings file.
		/// </summary>
		/// <seealso cref="SettingFileLocationFor(Module)"/>
		/// <seealso cref="SettingFileLocationFor(Type)"/>
		/// <seealso cref="SettingFileLocationFor(Assembly)"/>
		/// <seealso cref="SettingFileLocationFor(AssemblyName)"/>
		/// <seealso cref="SettingFolderLocationFor(Module)"/>
		/// <seealso cref="SettingFolderLocationFor(Type)"/>
		/// <seealso cref="SettingFolderLocationFor(AssemblyName)"/>
		/// <param name="asm">The <see cref="Assembly"/> of the caller.</param>
		/// <returns>A string that points to the folder containing the settings file.</returns>
		public static string SettingFolderLocationFor( Assembly asm ) => SettingFolderLocationFor( asm.GetName() );

		/// <summary>
		/// Creates a path that points to the folder containing the settings file.
		/// </summary>
		/// <seealso cref="SettingFileLocationFor(Module)"/>
		/// <seealso cref="SettingFileLocationFor(Type)"/>
		/// <seealso cref="SettingFileLocationFor(Assembly)"/>
		/// <seealso cref="SettingFileLocationFor(AssemblyName)"/>
		/// <seealso cref="SettingFolderLocationFor(Module)"/>
		/// <seealso cref="SettingFolderLocationFor(Type)"/>
		/// <seealso cref="SettingFolderLocationFor(Assembly)"/>
		/// <param name="asmName">The <see cref="AssemblyName"/> of the caller's assembly.</param>
		/// <returns>A string that points to the folder containing the settings file.</returns>
		public static string SettingFolderLocationFor( AssemblyName asmName ) => SettingFolderLocationFor( asmName.Name );

		private static string SettingFolderLocationFor( string asmName ) => Path.Combine( SETTINGS_FOLDER, asmName );
		#endregion

		[SuppressMessage( "Style", "IDE0066:Convert switch statement to expression", Justification = "Complaining I do not have all enums there." )]
		static SettingManager() {
			switch ( Environment.OSVersion.Platform ) {
				case PlatformID.Win32NT:
					SETTINGS_FOLDER = @"C:\ProgramData\FluffyBot\Settings\";
					break;
				case PlatformID.Unix:
					SETTINGS_FOLDER = "/etc/fluffybot/";
					break;
				default:
					SETTINGS_FOLDER = "./Settings/";
					break;
			}
		}

		// TODO: Document the `this` properies.
		#region This Properties

		public object this[Module module, string key] {
			get => GetSetting( module, key );
			set => SetSetting( module, key, value );
		}

		public object this[Type mod, string key] {
			get => GetSetting( mod, key );
			set => SetSetting( mod, key, value );
		}

		public object this[Assembly asm, string key] {
			get => GetSetting( asm, key );
			set => SetSetting( asm, key, value );
		}

		public object this[AssemblyName asmName, string key] {
			get => GetSetting( asmName, key );
			set => SetSetting( asmName, key, value );
		}

		#endregion

		private Dictionary<string, Dictionary<string, object>> settings = new Dictionary<string, Dictionary<string, object>>();

		private SettingManager() {
			ModuleHandler.NewModuleLoaded += ( asm, _ ) => LoadModuleSettings( asm.GetName() );
			AppDomain.CurrentDomain.ProcessExit += ( _0, _1 ) => BotClosing();
		}

		#region Methods

		// TODO: Write documentation on the methods below this comment.
		#region Public Methods

		#region Get Methods

		#region Get Settings

		public object GetSetting( Module module, string key ) => GetSetting( module.GetType(), key );

		public object GetSetting( Type type, string key ) => GetSetting( type.Assembly, key );

		public object GetSetting( Assembly asm, string key ) => GetSetting( asm.GetName(), key );

		public object GetSetting( AssemblyName asmName, string key ) => settings[asmName.Name][key];

		#endregion

		#region Try Get Settings

		public bool TryGetSetting( Module module, string key, out object value ) => TryGetSetting( module.GetType(), key, out value );

		public bool TryGetSetting( Type type, string key, out object value ) => TryGetSetting( type.Assembly, key, out value );

		public bool TryGetSetting( Assembly asm, string key, out object value ) => TryGetSetting( asm.GetName(), key, out value );

		public bool TryGetSetting( AssemblyName asmName, string key, out object value ) {
			value = null;

			if ( !settings.TryGetValue( asmName.Name, out var modSettings ) ) {
				return false;
			}

			return modSettings.TryGetValue( key, out value );
		}

		#endregion

		#region Try Get Settings of T

		public bool TryGetSetting<T>( Module module, string key, out T value ) => TryGetSetting( module.GetType(), key, out value );

		public bool TryGetSetting<T>( Type type, string key, out T value ) => TryGetSetting( type.Assembly, key, out value );

		public bool TryGetSetting<T>( Assembly asm, string key, out T value ) => TryGetSetting( asm.GetName(), key, out value );

		public bool TryGetSetting<T>( AssemblyName asmName, string key, out T value ) {
			value = default;
			var success = TryGetSetting( asmName, key, out var objValue );

			if ( !success ) {
				return false;
			}

			try {
				value = ( T )Convert.ChangeType( objValue, typeof( T ) );
			} catch {
				return false;
			}

			return true;
		}

		#endregion

		#endregion

		#region Set Methods

		#region Set Settings

		public void SetSetting( Module module, string key, object value ) => SetSetting( module.GetType(), key, value );

		public void SetSetting( Type type, string key, object value ) => SetSetting( type.Assembly, key, value );

		public void SetSetting( Assembly asm, string key, object value ) => SetSetting( asm.GetName(), key, value );

		public void SetSetting( AssemblyName asmName, string key, object value ) => settings[asmName.Name][key] = value;

		#endregion

		#region Try Set Settings

		public bool TrySetSetting( Module module, string key, object value ) => TrySetSetting( module.GetType(), key, value );

		public bool TrySetSetting( Type type, string key, object value ) => TrySetSetting( type.Assembly, key, value );

		public bool TrySetSetting( Assembly asm, string key, object value ) => TrySetSetting( asm.GetName(), key, value );

		public bool TrySetSetting( AssemblyName asmName, string key, object value ) {
			if ( !settings.ContainsKey( asmName.Name ) || !settings[asmName.Name].ContainsKey( key ) ) {
				return false;
			}

			settings[asmName.Name][key] = value;

			return true;
		}

		#endregion

		#endregion

		#region Add Methods

		#region Add Settings

		public void AddSetting( Module module, string key, object value ) => AddSetting( module.GetType(), key, value );

		public void AddSetting( Type type, string key, object value ) => AddSetting( type.Assembly, key, value );

		public void AddSetting( Assembly asm, string key, object value ) => AddSetting( asm.GetName(), key, value );

		public void AddSetting( AssemblyName asmName, string key, object value ) {
			var newDic = new Dictionary<string, object>();

			settings.Add( asmName.Name, newDic );

			newDic.Add( key, value );
		}

		#endregion

		#region Try Add Settings

		public bool TryAddSetting( Module module, string key, object value ) => TryAddSetting( module.GetType(), key, value );

		public bool TryAddSetting( Type type, string key, object value ) => TryAddSetting( type.Assembly, key, value );

		public bool TryAddSetting( Assembly asm, string key, object value ) => TryAddSetting( asm.GetName(), key, value );

		public bool TryAddSetting( AssemblyName asmName, string key, object value ) {
			Dictionary<string, object> modSettings;

			if ( settings.ContainsKey( asmName.Name ) ) {
				modSettings = settings[asmName.Name];
			} else {
				modSettings = new Dictionary<string, object>();

				settings.Add( asmName.Name, modSettings );
			}

			return modSettings.TryAdd( key, value );
		}

		#endregion

		#endregion

		#region Contains Methods

		#region Contains Mod Settings

		public bool ContainsModSettings( Module module ) => ContainsModSettings( module.GetType() );

		public bool ContainsModSettings( Type type ) => ContainsModSettings( type.Assembly );

		public bool ContainsModSettings( Assembly asm ) => ContainsModSettings( asm.GetName() );

		public bool ContainsModSettings( AssemblyName asmName ) => settings.ContainsKey( asmName.Name );

		#endregion

		#region Contains Setting Key

		public bool ContainsSettingKey( Module module, string key ) => ContainsSettingKey( module.GetType(), key );

		public bool ContainsSettingKey( Type type, string key ) => ContainsSettingKey( type.Assembly, key );

		public bool ContainsSettingKey( Assembly asm, string key ) => ContainsSettingKey( asm.GetName(), key );

		public bool ContainsSettingKey( AssemblyName asmName, string key ) {
			if ( !ContainsModSettings( asmName ) ) {
				return false;
			}

			return settings[asmName.Name].ContainsKey( key );
		}

		#endregion

		#region Contains Setting Value

		public bool ContainsSettingValue( Module module, object value ) => ContainsSettingValue( module.GetType(), value );

		public bool ContainsSettingValue( Type type, object value ) => ContainsSettingValue( type.Assembly, value );

		public bool ContainsSettingValue( Assembly asm, object value ) => ContainsSettingValue( asm.GetName(), value );

		public bool ContainsSettingValue( AssemblyName asmName, object value ) {
			if ( !ContainsModSettings( asmName ) ) {
				return false;
			}

			return settings[asmName.Name].ContainsValue( value );
		}

		#endregion

		#endregion

		#endregion

		private void LoadModuleSettings( AssemblyName asmName ) {
			var moduleSettings = SettingFileLocationFor( asmName );

			if ( !File.Exists( moduleSettings ) ) {
				settings.Add( asmName.Name, new Dictionary<string, object>() );
				return;
			}

			var settingStream = new FileStream( moduleSettings, FileMode.Open, FileAccess.Read, FileShare.Read );
			var reader = new StreamReader( settingStream, Encoding.BigEndianUnicode );
			var json = reader.ReadToEnd();
			var settingDic = JsonConvert.DeserializeObject<Dictionary<string, object>>( json, JSON_SETTINGS );

			settings.Add( asmName.Name, settingDic );
		}

		private void BotClosing() {
			foreach ( var pair in settings ) {
				var file = SettingFileLocationFor( pair.Key );

				if ( !File.Exists( file ) ) {
					var parent = new FileInfo( file ).Directory.FullName;

					if ( !Directory.Exists( parent ) ) {
						_ = Directory.CreateDirectory( parent );
					}
				}

				var settingFile = new FileStream( file, FileMode.Create, FileAccess.Write, FileShare.Read );
				var writer = new StreamWriter( settingFile, Encoding.BigEndianUnicode );
				var json = JsonConvert.SerializeObject( pair.Value, JSON_SETTINGS );

				writer.Write( json );
				writer.Flush();
				writer.Dispose();
			}
		}

		#endregion
	}
}