﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace FluffyBot.Utilities.Logging {
	public static class LogHandler {

		public delegate void WroteNewLogMessageOut( LogMessage logMessage );

		public static readonly string LOG_LOCATION;
		public static readonly string EXTRA_LOG_LOCATION;

		public static readonly JsonSerializerSettings DEFAULT_LOG_OPTIONS = new JsonSerializerSettings {
			ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor,
			DateFormatHandling = DateFormatHandling.IsoDateFormat,
			DateParseHandling = DateParseHandling.DateTimeOffset,
			DefaultValueHandling = DefaultValueHandling.Ignore,
			FloatFormatHandling = FloatFormatHandling.DefaultValue,
			FloatParseHandling = FloatParseHandling.Double,
			Formatting = Formatting.Indented,
			MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
			MissingMemberHandling = MissingMemberHandling.Ignore,
			NullValueHandling = NullValueHandling.Ignore,
			StringEscapeHandling = StringEscapeHandling.EscapeHtml,
			TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Simple,
			TypeNameHandling = TypeNameHandling.None
		};

		public static WroteNewLogMessageOut NewLogMessageOut;

		private static ConcurrentQueue<LogMessage> logQueue;
		private static ManualResetEvent newLog;
		private static ManualResetEvent closing;
		private static StreamWriter outputLog;
		private static Task loggingTask;

		[SuppressMessage( "Style", "IDE0066:Convert switch statement to expression", Justification = "Complaining I do not have all enums there." )]
		static LogHandler() {
			logQueue = new ConcurrentQueue<LogMessage>();
			newLog = new ManualResetEvent( false );
			closing = new ManualResetEvent( false );

			switch ( Environment.OSVersion.Platform ) {
				case PlatformID.Win32NT:
					LOG_LOCATION = @"C:\ProgramData\FluffyBot\discord.log";
					break;
				case PlatformID.Unix:
					LOG_LOCATION = "/var/log/fluffybot.json";
					break;
				default:
					LOG_LOCATION = "./discord.log";
					break;
			}

			EXTRA_LOG_LOCATION = Path.Combine( Path.GetDirectoryName( LOG_LOCATION ), "fluffybot-extra" );

			if ( !Directory.Exists( EXTRA_LOG_LOCATION ) ) {
				_ = Directory.CreateDirectory( EXTRA_LOG_LOCATION );
			}

			if ( Environment.GetCommandLineArgs().Contains( "--print-to-console" ) ) {
				NewLogMessageOut += WriteLogToConsole;
			}

			var fileStream = new FileStream( LOG_LOCATION, FileMode.Create, FileAccess.Write, FileShare.Read );

			outputLog = new StreamWriter( fileStream, Encoding.BigEndianUnicode ) {
				NewLine = "\n",
				AutoFlush = false
			};

			loggingTask = new Task( WriteLogs, TaskCreationOptions.RunContinuationsAsynchronously );
			loggingTask.Start();

			AppDomain.CurrentDomain.ProcessExit += ( _, e ) => {
				closing.Set();

				loggingTask.Wait();

				loggingTask.Dispose();
			};
		}

		public static void LogExcption( string message, Exception e ) => Log( new LogMessage( message, Severity.Exception, DateTimeOffset.Now, e, new StackTrace( e ) ) );

		public static void LogError( string message, Exception e ) => Log( new LogMessage( message, Severity.Error, DateTimeOffset.Now, e, new StackTrace( e ) ) );

		public static void LogWarning( string message ) => Log( message, Severity.Warning );

		public static void Log( string message, Severity severity = Severity.Normal ) {
			var stackTrace = new StackTrace();
			var i = 0;

			for ( /* `i` is already declared. */; i < stackTrace.FrameCount; ++i ) {
				var frame = stackTrace.GetFrame( i );

				if ( frame.GetMethod()?.DeclaringType != typeof( LogHandler ) ) {
					break;
				}
			}

			Log( new LogMessage( message, severity, DateTimeOffset.Now, null, new StackTrace( i ) ) );
		}

		public static void Log( string message, Exception e, StackTrace stackTrace, DateTimeOffset dateTime, Severity severity = Severity.Normal ) =>
			Log( new LogMessage( message, severity, dateTime, e, stackTrace ) );

		public static void Log( LogMessage message ) {
			logQueue.Enqueue( message );

			newLog.Set();
		}

		private static void WriteLogToConsole( LogMessage log ) {
			Console.Write( '[' );

			switch ( log.Severity ) {
				case Severity.Normal:
					Console.ForegroundColor = ConsoleColor.Green;
					break;
				case Severity.Warning:
					Console.ForegroundColor = ConsoleColor.Yellow;
					break;
				case Severity.Error:
				case Severity.Exception:
					Console.ForegroundColor = ConsoleColor.Red;
					break;
			}

			Console.Write( Enum.GetName( typeof( Severity ), log.Severity ) );

			Console.ResetColor();

			Console.WriteLine( $" / {log.Occured.ToString()}]: {log.Message}" );

			if ( log.Severity == Severity.Error || log.Severity == Severity.Exception ) {
				if ( log.Exception is Exception ) {
					Console.WriteLine( $"Exception: {log.Exception.GetType().FullName}\nMessage: {log.Exception.Message}" );
				}

				if ( !string.IsNullOrWhiteSpace( log.Stacktrace ) ) {
					Console.WriteLine( $"Stacktrace:\n{log.Stacktrace}" );
				}
			}
		}

		private static void WriteLogs() {
			var waitHandles = new[] { newLog, closing };

			while ( true ) {
				var exeNum = WaitHandle.WaitAny( waitHandles );

				switch ( exeNum ) {
					case 0:
						newLog.Reset();

						WriteOut();

						break;
					case 1:
						closing.Reset();

						WriteOut();

						outputLog.Dispose();

						return;
				}
			}
		}

		private static void WriteOut() {
			if ( outputLog.BaseStream.Position == 0 ) {
				var start = Encoding.BigEndianUnicode.GetBytes( "[\n\t" );

				outputLog.Write( start );
			} else {
				outputLog.BaseStream.Position -= outputLog.Encoding.GetMaxByteCount( 1 );
			}

			while ( logQueue.TryDequeue( out var log ) ) {
				var logJson = JsonConvert.SerializeObject( log, DEFAULT_LOG_OPTIONS )
					.Replace( "\n", "\n\t" );

				outputLog.Write( '\t' );
				outputLog.Write( logJson );
				outputLog.WriteLine( ',' );

				outputLog.Flush();

				NewLogMessageOut?.Invoke( log );
			}

			outputLog.Write( ']' );

			outputLog.Flush();
		}
	}
}