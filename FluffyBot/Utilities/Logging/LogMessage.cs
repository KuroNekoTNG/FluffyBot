﻿using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Text.Json.Serialization;

namespace FluffyBot.Utilities.Logging {

	/// <summary>
	/// Used to tell how severe the log is.
	/// </summary>
	public enum Severity : byte {
		/// <summary>
		/// Inidcates that the problem is nothing.
		/// </summary>
		Normal = 0,
		/// <summary>
		/// Something happened that was not expected, but not to worry.
		/// </summary>
		Warning = 1,
		/// <summary>
		/// Something happened that might be needing an eye on.
		/// </summary>
		Error = 2,
		/// <summary>
		/// Holy shit, what the fuck just happened!?
		/// </summary>
		Exception = 3
	}

	[SuppressMessage( "Performance", "CA1815:Override equals and operator equals on value types", Justification = "No since this struct holds data." )]
	public readonly struct LogMessage {

		public string Message {
			get;
		}

		[JsonIgnore]
		public string Stacktrace {
			get {
				if ( Exception is Exception ) {
					return Exception.StackTrace;
				}

				if ( StackTrace is StackTrace ) {
					return StackTrace.ToString();
				}

				return string.Empty;
			}
		}

		public Severity Severity {
			get;
		}

		public DateTimeOffset Occured {
			get;
		}

		public Exception Exception {
			get;
		}

		[JsonIgnore]
		public StackTrace StackTrace {
			get;
		}

		public LogMessage( string message, Severity severity, DateTimeOffset occured, Exception exception, StackTrace stackTrace ) {
			Message = message;
			Severity = severity;
			Occured = occured;
			Exception = exception;
			StackTrace = stackTrace;
		}
	}
}