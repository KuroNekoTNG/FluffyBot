﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage(
	"Performance",
	"CA1810:Initialize reference type static fields inline",
	Justification = "Sometimes a static constructor is needed." )]
[assembly: SuppressMessage(
	"Design",
	"CA1031:Do not catch general exception types",
	Justification = "Sometimes there is no need to handle any exceptions, and sometimes there is a need to catch general." )]
[assembly: SuppressMessage( "Style", "IDE1006:Naming Styles", Justification = "Must mimic how it is in libc.", Scope = "member", Target = "~M:FluffyBot.Program.setgid(System.UInt32)~System.Int32" )]
[assembly: SuppressMessage( "Style", "IDE1006:Naming Styles", Justification = "Must mimic how it is in libc.", Scope = "member", Target = "~M:FluffyBot.Program.setuid(System.UInt32)~System.Int32" )]
[assembly: SuppressMessage( "Style", "IDE1006:Naming Styles", Justification = "Must mimic how it is in libc.", Scope = "member", Target = "~M:FluffyBot.Program.getuid()~System.Int32" )]
[assembly: SuppressMessage( "Style", "IDE1006:Naming Styles", Justification = "Must mimic how it is in libc.", Scope = "member", Target = "~M:FluffyBot.Program.sys_chmod(System.String,System.UInt32)~System.Int32" )]
[assembly: SuppressMessage( "Style", "IDE1006:Naming Styles", Justification = "Must mimic how it is in libc.", Scope = "member", Target = "~M:FluffyBot.Program.chown(System.String,System.UInt32,System.UInt32)~System.Int32" )]
