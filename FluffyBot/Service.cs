﻿using System.ServiceProcess;

using FluffyBot.Core;

namespace FluffyBot {
	public sealed class Service : ServiceBase {

		public Service() {
			ServiceName = "FluffyBot";
			CanShutdown = true;
			CanStop = false;
		}

		protected override void OnStart( string[] args ) => Program.CommonStart( args );

		protected override void OnShutdown() {
			var client = DiscordController.Controller.Client;

			client.LogoutAsync().Wait();
			client.StopAsync().Wait();
			client.Dispose();

			base.OnShutdown();
		}
	}
}
