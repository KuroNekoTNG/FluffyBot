# Why are Unix builds single file, while Windows aren't?
The reason behind Unix builds being single-file is that Unix systems will have an executable that calls libraries.
In the case of .NET, it started as a Windows-centric platform. On Windows, programs expect libraries they use to be in the same location as them unless it was part of the OS.
*nix oses work differently (will, maybe not macOS, Apple prefers to go down their own route with things) by having libraries in places like `/lib`, `/lib64`, `/usr/lib`, and `/usr/lib64`.
.NET at start will not like this, again, because it started as a Windows-centric platform. So it is expecting that all libraries are in the same folder as it or already in the AppDomain.
In order to make it not seem out of the ordinary, I've made it to where *nix system publishes will produce a single-file.
Windows is left with all of the usual files all laid out into the file system.
# Why am I seeing builds here, and not under release/tags?
Simple, GitLab does not allow the size of FluffyBot, even in `.rar`, `.7z`, or `.zip` because of size.
To get around this, published builds are placed here and are commited to the repo.
If there is another way to get around this without the user having to build it, I am all ears.
# What is with the differnt style of executable files?
Windows is actually the odd one out here. This is because on Windows, it is common for programs to have their own folder, and in this folder they can have as many files as they want.
On *nix systems, however, it is more common to have a single executable file than having it in a single directory with tons of libraries.