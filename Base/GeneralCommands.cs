﻿using System;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

using Discord;
using Discord.Commands;

using FluffyBot.Database;

using Image = FluffyBot.Database.Image;

namespace FluffyBot.Core.BaseCommands {
	public class GeneralCommands : CommandCore {

		[Command( "hwinfo", true, RunMode = RunMode.Async )]
		public Task MyHardware() {
			var osArch = RuntimeInformation.OSArchitecture;
			var procArch = RuntimeInformation.ProcessArchitecture;
			var osDesc = RuntimeInformation.OSDescription;
			var frameworkDesc = RuntimeInformation.FrameworkDescription;
			var embedBuilder = new EmbedBuilder() {
				Color = Color.DarkPurple,
				Description = "Information about me.",
				Timestamp = DateTimeOffset.Now,
				Title = "Current Hardware/Software Infomation"
			};

			embedBuilder.AddField( "OS Architecture", osArch );
			embedBuilder.AddField( "OS Desc", osDesc );
			embedBuilder.AddField( "Process Architecture", procArch );
			embedBuilder.AddField( "Framework Desc", frameworkDesc );

			return Context.Channel.SendMessageAsync( embed: embedBuilder.Build() );
		}

		[Command( "img", RunMode = RunMode.Async )]
		public Task PrintImage( string name ) {
			var img = default( Image );

			using ( var dbContext = new FluffyBotContext() ) {
				foreach ( var dbImg in dbContext.Image ) {
					if ( dbImg.GuildID == Context.Guild.Id && dbImg.Name == name ) {
						img = dbImg;
						break;
					}
				}
			}

			if ( img is null ) {
				return Context.Channel.SendMessageAsync( $"No image by the name of `{name}` could be found." );
			}

			var embedBuilder = new EmbedBuilder {
				Title = $"***{name}***",
				ImageUrl = img.Url
			};

			return Context.Channel.SendMessageAsync( embed: embedBuilder.Build() );
		}

		[Command( "setImg", RunMode = RunMode.Async ), RequireUserPermission( GuildPermission.ManageEmojis )]
		public Task SetImg( string name, string url ) {
			if ( name.Length > 16 ) {
				return Context.Channel.SendMessageAsync( $"Sorry, but the name is too long. Max 16 characters." );
			}

			var img = new Image {
				GuildID = Context.Guild.Id,
				Name = name,
				Url = url
			};

			using ( var dbContext = new FluffyBotContext() ) {
				dbContext.Image.Add( img );

				dbContext.SaveChanges();
			}

			return Context.Channel.SendMessageAsync( $"Success! Image is now able to be shown via `{name}`." );
		}

		[Command( "listImg", RunMode = RunMode.Async )]
		public async Task ListImages() {
			var embedBuilder = new EmbedBuilder();
			var images = default( Image[] );
			var typingState = Context.Channel.EnterTypingState();

			using ( var dbContext = new FluffyBotContext() ) {
				images = dbContext.Image.ToArray();
			}

			for ( var i = 0; i < images.Length; ++i ) {
				var image = images[i];

				embedBuilder.AddField( $"Image {i}", image.Name, true );

				if ( ( i != 0 && i % 25 == 0 ) || i == images.Length - 1 ) {
					embedBuilder.Title = $"{i - embedBuilder.Fields.Count} - {i}";

					await Context.Channel.SendMessageAsync( embed: embedBuilder.Build() );

					embedBuilder.Fields.Clear();
				}
			}

			typingState.Dispose();
		}

		[Command( "latency", true, RunMode = RunMode.Sync )]
		public Task PingMe() => Context.Channel.SendMessageAsync( $"Latency: {Context.Client.Latency} ms" );

		[Command( "ping", true, RunMode = RunMode.Async )]
		public Task Ping() => Context.User.SendMessageAsync( "Pong" );

		/// <summary>
		/// Links the caller to the source code of the bot.
		/// </summary>
		/// <returns></returns>
		[Command( "showmecode", RunMode = RunMode.Async )]
		public Task ShowMeCode() => Context.Channel.SendMessageAsync( $"Here is my source code, UwU: https://gitlab.com/KuroNekoTNG/FluffyBot" );

		/// <summary>
		/// This command returns the user's stats.
		/// </summary>
		/// <returns></returns>
		[Command( "status", false, RunMode = RunMode.Async )]
		public Task Status() {
			string lastWarningDate, reason;
			User user;

			var embedBuilder = new EmbedBuilder() {
				Timestamp = DateTimeOffset.Now,
				Color = Color.DarkRed,
				ThumbnailUrl = Context.User.GetAvatarUrl()
			};

			using ( var context = new FluffyBotContext() ) {
				user = context.Find<User>( Context.User.Id );
			}

			if ( user is null ) {
				user = new User() {
					DiscordId = Context.User.Id
				};

				using ( var context = new FluffyBotContext() ) {
					context.Add( user );

					context.SaveChanges( true );
				}
			}

			lastWarningDate = user.LastWarning == null ? "N/A" : user.LastWarning.ToString();
			reason = string.IsNullOrWhiteSpace( user.Reason ) ? "N/A" : user.Reason;

			embedBuilder.AddField( "Points", user.Points, true );
			embedBuilder.AddField( "Adult Permission", user.GetAdultAsBool(), true );
			embedBuilder.AddField( "Last Warning", lastWarningDate, true );
			embedBuilder.AddField( "Warning Reason", reason, true );

			return Context.Channel.SendMessageAsync( embed: embedBuilder.Build() );
		}
	}
}