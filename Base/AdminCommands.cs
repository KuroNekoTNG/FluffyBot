﻿using System.Threading.Tasks;

using Discord;
using Discord.Commands;
using Discord.WebSocket;

using FluffyBot.Database;

namespace FluffyBot.Core.BaseCommands {
	public class AdminCommands : CommandCore {

		#region Static Methods

		static AdminCommands() {
			DiscordController.Controller.UserBanned += UserBanned;
			DiscordController.Controller.UserUnbanned += UserUnbanned;
			DiscordController.Controller.UserJoined += UserJoined;
			DiscordController.Controller.UserLeft += UserLeft;
		}

		private static Task UserJoined( SocketGuildUser user ) => UserTransaction( "Let's all welcome our new member, {0}!", "User `{0}` has joined the server.", user, user.Guild );

		private static Task UserLeft( SocketGuildUser user ) => UserTransaction( "UwU We have lost a member, goodbye `{0}`.", "User `{0}` has left the server.", user, user.Guild );

		private static Task UserBanned( SocketUser user, SocketGuild guild ) => UserTransaction(
			"Attention! User `{0}` has been banned, no one else make their mistake.",
			"User `{0}` has been banned from the server.",
			user,
			guild
		);

		private static Task UserUnbanned( SocketUser user, SocketGuild guild ) => UserTransaction(
			"The previously banned user `{0}` has been pardoned, we're watching you `{0}`.",
			"User `{0}` has been unbanned from the server.",
			user,
			guild
		);

		private static async Task UserTransaction( string lobbyFormat, string transactFormat, SocketUser user, SocketGuild guild ) {
			if ( !GuildSetChannels( guild, out var lobby, out var transact ) ) {
				return;
			}

			if ( transact != default && !string.IsNullOrWhiteSpace( transactFormat ) ) {
				await transact.SendMessageAsync( string.Format( transactFormat, user.Username ) );
			}

			if ( lobby != default ) {
				await lobby.SendMessageAsync( string.Format( lobbyFormat, user.Username ) );
			}
		}

		private static bool GuildSetChannels( SocketGuild guild, out ISocketMessageChannel lobby, out ISocketMessageChannel transaction ) {
			lobby = default;
			transaction = default;

			using ( var context = new FluffyBotContext() ) {
				var guildInfo = context.Guilds.Find( guild.Id );

				if ( guildInfo is null ) {
					context.Guilds.Add( new GuildInfo {
						GuildID = guild.Id
					} );

					context.SaveChanges();

					return false;
				}

				if ( guildInfo.LobbyID is ulong lobbyId ) {
					lobby = guild.GetTextChannel( lobbyId );
				}

				if ( guildInfo.TransactionChannelID is ulong transactId ) {
					transaction = guild.GetTextChannel( transactId );
				}
			}

			// Since this method returns false if either the transaction channel and the lobby channel are not set.
			return lobby is ISocketMessageChannel || transaction is ISocketMessageChannel;
		}

		#endregion

		[Command( "setAsLobby", true, RunMode = RunMode.Async ), RequireUserPermission( GuildPermission.Administrator )]
		public Task SetAsLobby() {
			using ( var dbContext = new FluffyBotContext() ) {
				var guild = dbContext.Guilds.Find( Context.Guild.Id );

				if ( guild is null ) {
					guild = new GuildInfo {
						GuildID = Context.Guild.Id
					};

					dbContext.Guilds.Add( guild );
				}

				guild.LobbyID = Context.Channel.Id;

				return dbContext.SaveChangesAsync();
			}
		}

		[Command( "setAsTransact", true, RunMode = RunMode.Async ), RequireUserPermission( GuildPermission.Administrator )]
		public Task SetAsTransaction() {
			using ( var dbContext = new FluffyBotContext() ) {
				var guild = dbContext.Guilds.Find( Context.Guild.Id );

				if ( guild is null ) {
					guild = new GuildInfo {
						GuildID = Context.Guild.Id
					};

					dbContext.Guilds.Add( guild );
				}

				guild.TransactionChannelID = Context.Channel.Id;

				return dbContext.SaveChangesAsync();
			}
		}
	}
}
