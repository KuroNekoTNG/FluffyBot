﻿using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Security.Principal;

namespace FluffyBotInstaller.NT {

	public static class Installer {

		public static bool Admin {
			get {
				var id = WindowsIdentity.GetCurrent();
				var principal = new WindowsPrincipal( id );

				return principal.IsInRole( WindowsBuiltInRole.Administrator );
			}
		}

		public static void StartInstallProgress() {
			if ( !Admin ) {
				ConsoleManager.WriteLine( "Please run as admin.", Level.Bad );
				return;
			}

			var path = Assembly.GetAssembly( typeof( Installer ) ).Location;
			path = Path.GetDirectoryName( path );

			foreach ( var info in new DirectoryInfo( path ).EnumerateFileSystemInfos() ) {
				if ( info.Attributes.HasFlag( FileAttributes.Directory ) ) {
					CopyToLocation( ( DirectoryInfo )info, Path.Combine( @"C:\Program Files\FluffyBot", info.Name ) );
				}
			}

			var serviceInstaller = new Process() {
				StartInfo = {
					FileName = "sc.exe",
					Arguments = @"create FluffyBot binpath=""C:\Program Files\FluffyBot\FluffyBot.exe""",
					RedirectStandardError = true,
					RedirectStandardOutput = true
				}
			};

			serviceInstaller.ErrorDataReceived += ( _, data ) => ConsoleManager.WriteLine( data.Data, Level.Bad );
			serviceInstaller.OutputDataReceived += ( _, data ) => ConsoleManager.WriteLine( data.Data );

			serviceInstaller.Start();
			serviceInstaller.WaitForExit();

			ConsoleManager.WriteLine( "Please restart your computer." );

			static void CopyToLocation( DirectoryInfo dir, string to ) {
				if ( !Directory.Exists( to ) ) {
					_ = Directory.CreateDirectory( to );
				}

				foreach ( var info in dir.EnumerateFileSystemInfos() ) {
					if ( info.Attributes.HasFlag( FileAttributes.Directory ) ) {
						ConsoleManager.WriteLine( $"`{info.FullName}` is directory, copying files inside to destination." );

						CopyToLocation( ( DirectoryInfo )info, Path.Combine( to, info.Name ) );
					} else {
						ConsoleManager.WriteLine( $"`{info.FullName}` is file, copying to `{to}`." );

						File.Copy( info.FullName, Path.Combine( to, info.Name ), true );
					}
				}
			}
		}

	}
}