﻿using System;
using System.Collections.Generic;
using System.IO;

namespace FluffyBotInstaller {
	internal static class GeneralConfig {

		public static void CreateBasicConfig( string configLoc ) {
			using ( var fileStream = new FileStream( configLoc, FileMode.Create, FileAccess.Write, FileShare.Read ) ) {
				using ( var writer = new StreamWriter( fileStream ) ) {
					WriteInfoToConfig( writer );
				}
			}
		}

		private static void WriteInfoToConfig( StreamWriter writer ) {
			var bannedWords = new List<string>();

			ConsoleManager.Write( "Please provide Discord API Token: " );
			Console.ResetColor();

			var token = Console.ReadLine();

			ConsoleManager.WriteLine( "Input a list of banned words, once done, just press [ENTER]:" );

			do {
				var word = Console.ReadLine();

				if ( string.IsNullOrWhiteSpace( word ) ) {
					break;
				}

				bannedWords.Add( word );
			} while ( true );

			writer.Write( $@"[""token"": ""{token}"", ""bannedKeywords"": {{ ""$type"": ""{bannedWords.GetType().FullName}"", ""$values"": [" );

			for ( var i = 0; i < bannedWords.Count; ++i ) {
				writer.Write( $@"""{bannedWords[i]}""" );

				if ( i < bannedWords.Count - 1 ) {
					writer.Write( ", " );
				}
			}

			writer.Write( $@" ] }} ]" );

			writer.Flush();
		}
	}
}