﻿using System;

namespace FluffyBotInstaller {

	internal enum Level : sbyte {
		Bad = -1,
		Neutral = 0,
		Good = 1
	}

	internal static class ConsoleManager {

		public static void WriteLine( string message, Level level = Level.Neutral ) => Write( message + '\n', level );

		public static void Write( string message, Level level = Level.Neutral ) {
			switch ( level ) {
				default:
				case Level.Neutral:
					Console.ResetColor();
					break;
				case Level.Bad:
					Console.ForegroundColor = ConsoleColor.Red;
					break;
				case Level.Good:
					Console.ForegroundColor = ConsoleColor.Green;
					break;
			}

			Console.Write( message );
		}
	}
}