﻿using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Runtime.InteropServices;

using FluffyBot.Database;
using FluffyBot.Utilities;
using FluffyBot.Utilities.Extension;
using FluffyBot.Utilities.Logging;

namespace FluffyBotInstaller.Posix {
	/* Due to .NET originating from Windows New Technology,
	 * native POSIX support is basically non-existant, at least to my knowledge.
	 * As such, this static class will include needed external calls to Unix(-like)
	 * calls. Such as geteuid(). */
	internal static class Installer {

		public static void StartInsallProgress() {
			if ( geteuid() != 0 ) {
				throw new IOException( "Cannot proceed unless root." );
			}

			ConsoleManager.WriteLine( "Creating Folders needed." );
			CreateFolderStructure();

			ConsoleManager.WriteLine( "Creating a secure environment." );
			CreateUser();

			ConsoleManager.WriteLine( "Changing permissions of folder." );
			ChangeFolderPermissions();

			ConsoleManager.WriteLine( "Creating Daemon for Systemd." );
			CreateServiceFile();

			ConsoleManager.WriteLine( "Reloading Systemd." );
			ReloadSystemd();

			ConsoleManager.WriteLine( "FluffyBot is almost ready.\nGathering info for config." );
			GeneralConfig.CreateBasicConfig( "/etc/fluffybot/core.json" );
		}

		private static void ChangeFolderPermissions() {
			var locations = GetLocations();

			for ( var i = 0; i < locations.Length; ++i ) {
				var fluffyBotFolder = GetFluffyBotFolder( new DirectoryInfo( locations[i] ) );

				if ( fluffyBotFolder is null ) {
					continue;
				}

				GetFluffyBotID( out var uid, out var gid );

				SetChildPermissions( fluffyBotFolder, uid, gid );
			}

			static DirectoryInfo GetFluffyBotFolder( DirectoryInfo folder ) {
				if ( folder.Name.Equals( "FluffyBot", StringComparison.OrdinalIgnoreCase ) ) {
					return folder;
				}

				try {
					return GetFluffyBotFolder( folder.Parent );
				} catch {
					return null;
				}
			}

			static void SetChildPermissions( FileSystemInfo info, int uid, int gid ) {
				ConsoleManager.WriteLine( $"Setting `{info.FullName}` to {uid}:{gid}." );

				chown( info.FullName, uid, gid );

				if ( !info.Attributes.HasFlag( FileAttributes.Directory ) ) {
					return;
				}

				foreach ( var child in ( ( DirectoryInfo )info ).EnumerateFileSystemInfos() ) {
					SetChildPermissions( child, uid, gid );
				}
			}
		}

		private static void ReloadSystemd() {
			using ( var systemd = new Process() {
				StartInfo = {
					FileName = "systemctl",
					Arguments = "reload-daemon",
					RedirectStandardError = true,
					RedirectStandardOutput = true
				}
			} ) {
				systemd.ErrorDataReceived += ErrorDataRecieved;
				systemd.OutputDataReceived += OutputDataRecieved;

				systemd.Start();
				systemd.WaitForExit();
			}
		}

		private static void CreateServiceFile() {
			const string SERVICE_FILE_CONTENTS = @"[Unit]
Description=FluffyBot Discord Bot Service
# Ensures that the bot has access to the network.
After=network.target

[Service]
# Starts the bot executable.
ExecStart=/opt/fluffybot/FluffyBot
# Makes the bot not run as root.
User=fluffybot
Group=fluffybot

[Install]
# The thing to start the service with.
WantedBy=multi-user.target
";

			using ( var serviceFile = File.CreateText( "/etc/systemd/system/fluffybot.service" ) ) {
				serviceFile.Write( SERVICE_FILE_CONTENTS );
			}
		}

		private static void CreateUser() {
			using ( var userCreateProc = new Process() {
				StartInfo = {
					FileName = "useradd",
					Arguments = "-r -U -M -s /bin/false -c Discord\\ Bot fluffybot",
					RedirectStandardError = true,
					RedirectStandardOutput = true
				}
			} ) {
				userCreateProc.ErrorDataReceived += ErrorDataRecieved;
				userCreateProc.OutputDataReceived += OutputDataRecieved;

				userCreateProc.Start();
				userCreateProc.WaitForExit();
			}
		}

		private static void ErrorDataRecieved( object sender, DataReceivedEventArgs e ) => ConsoleManager.WriteLine( e.Data, Level.Bad );

		private static void OutputDataRecieved( object sender, DataReceivedEventArgs e ) => ConsoleManager.WriteLine( e.Data );

		private static void CreateFolderStructure() {
			var locations = GetLocations();

			for ( var i = 0; i < locations.Length; ++i ) {
				ConsoleManager.Write( $"Checking for existance of `{locations[i]}`. . ." );

				if ( !Directory.Exists( locations[i] ) ) {
					ConsoleManager.Write( "Not Found. . .Creating. . ." );

					try {
						_ = Directory.CreateDirectory( locations[i] );
					} catch ( Exception e ) {
						ConsoleManager.WriteLine( $"FAILED:\t{e.GetType().FullName}\t{e.Message}", Level.Bad );
						continue;
					}

					ConsoleManager.WriteLine( "SUCCESS!", Level.Good );
				} else {
					ConsoleManager.WriteLine( "Already Exists? Will use it.", Level.Bad );
				}
			}
		}

		private static string[] GetLocations() {
			var databaseLoc = Path.GetDirectoryName( FluffyBotContext.DatabaseFile );
			var logFolderLoc = Path.GetDirectoryName( LogHandler.LOG_LOCATION );
			var extLoc = ModuleHandler.MODULE_FOLDER;
			var settingsLoc = SettingManager.SETTINGS_FOLDER;

			return new[] { databaseLoc, logFolderLoc, extLoc, settingsLoc };
		}

		private static void GetFluffyBotID( out int uid, out int gid ) {
			var lines = File.ReadAllText( "/etc/passwd" ).Split( '\n' );
			// Will set them to a default.
			uid = -1;
			gid = -1;

			for ( var i = 0; i < lines.Length; ++i ) {
				var parts = lines[i].Split( ':' );

				if ( parts[i] == "fluffybot" && ( !int.TryParse( parts[2], out uid ) || !int.TryParse( parts[3], out gid ) ) ) {
					throw new Exception( "Failed to parse ID for FluffyBot." );
				}
			}
		}

		[DllImport( "libc.so.6", SetLastError = false )]
		[SuppressMessage( "Style", "IDE1006:Naming Styles", Justification = "This is how it looks for LibC." )]
		public static extern int geteuid();

		[DllImport( "libc.so.6", SetLastError = true, CharSet = CharSet.Unicode )]
		[SuppressMessage( "Style", "IDE1006:Naming Styles", Justification = "This is how it looks for LibC." )]
		public static extern int chown( string path, int owner, int group );
	}
}