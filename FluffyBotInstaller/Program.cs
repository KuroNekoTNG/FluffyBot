﻿using System;

namespace FluffyBotInstaller {

	internal static class Program {

		private static void Main() {
			switch ( Environment.OSVersion.Platform ) {
				case PlatformID.Win32NT:
					NT.Installer.StartInstallProgress();
					break;
				case PlatformID.Unix:
					Posix.Installer.StartInsallProgress();
					break;
				default:
					ConsoleManager.WriteLine( "Unknown PlatformID", Level.Bad );
					return;
			}
		}
	}
}
