# What is FluffyBot?
FluffyBot is a Discord bot that is a general purpose bot.
This means that it comes barebones, you can add or subtract modules from it.
This bot's original purpose was to learn how to use Discord.NET, it has since grown and now lives on my [Discord Server][0].

# Why would I want to use FluffyBot?
You would want to use it if you have ever just wanted to quickly get a Discord bot up and running without having to write from scratch.
This bot does have a "base" module that contains some basic calls. However, it is easy to add more modules to the bot.
This bot is also FOSS, if you couldn't tell. This means that the source is open for anyone to see.

# Why is it called FluffyBot?
Mainly because I am a furry and I am not creative when it comes to names. So FluffyBot it was called.

# What can this bot do?
As said earlier, it has a base module, however this module can be removed if you so as if choose.
You can expand what this bot does by simply making your own and placing that assembly in the respective folder:

Windows: C:\\ProgramData\FluffyBot\\Modules\\\[Module Name Here\]

Unix: /var/lib/fluffybot/\[Module Name Here\]

Unknown OS: ./Modules/\[Module Name Here\]

# How do I set up the bot?
The bot should be able to install itself if you run the FluffyBot Installer. However if you do not trust it, here is how you do it for Windows and Linux.
Good luck macOS people, I don't own a Mac, nor do I plan on it.

## Windows NT
### Install Location
The bot should be compiled using 64-bit compiling, and it is best to place it in a 64-bit environment. So place it in `C:\Program Files\FluffyBot`.
Once in there, you should be able to call it from anywhere.

### Setting up the service.
Open Powershell as admin and type the following:

```ps
New-Service -Name "FluffyBot" -BinaryPath "C:\Program Files\FluffyBot\FluffyBot.exe" -Description "FluffyBot is a general purpose Discord Bot." -StartupType System
```

Now the service has been created. If you ever decide to remove FluffyBot, simply do the following:

```ps
Remove-Service -Name "FluffyBot"
```

## Linux
### Required libraries
Before you can even run this bot, you will need some libraries installed first and foremost.
These are libraries (mostly) indirectly used by the bot, but are used by .NET Core.
The libraries are:
*  libc6
*  libgcc1
*  libgssapi-krb5-2
*  libicu
   *  For Ubuntu 14, use 52 (libicu52).
   *  For Ubuntu 16, use 55 (libicu55).
   *  For Ubuntu 17, use 57 (libicu57).
   *  For Ubuntu 18, use 60 (libicu60).
*  liblttng-ust0
*  libssl1.0.0
*  libstdc++6
*  zlib1g
*  libcurl
   *  For Ubuntu 14 and 16, use libcurl3.
   *  For Ubuntu 18, use libcurl4.

If you are running any of the following distros, follow Microsoft guidelines on [installing .NET Core][4]. This will install what the bot needs to run.
*  Red Hat Enterprise Linux 7 - x64
*  Red Hat Enterprise Linux 8.1 - x64
*  Ubuntu 16.04 - x64
*  Ubuntu 18.04 - x64
*  Ubuntu 19.04 - x64
*  Debian 9 - x64
*  Debian 10 - x64
*  Fedora 29 - x64
*  Fedora 30 - x64
*  CentOS 7 - x64
*  OpenSUSE 15 - x64
*  SLES 12 - x64
*  SLES 15 - x64

If you are using Arch, or any Arch-based Distro (which, you must be insane to use as a server), you will need to do:

```
yay -S dotnet-runtime-bin
```

This should install everything needed for the bot to run on Arch.

### Installing
With Linux, it is highly recommended to use the installer, there is a second option which is to use the simple install script [here][3].
Just make sure that you place FluffyBot in a location like `/usr/local/bin/FluffyBot` and/or symlinked the executable to `/usr/bin/fluffybot`.
You can do the following to run the scripts:

```
sudo -s -- 'curl -sSL https://gitlab.com/KuroNekoTNG/FluffyBot/snippets/1917991/raw -o - | bash'
```

If that does not work, since sometimes `sudo` is not compiled with some options, you can do this:

```
sudo -- sh -c 'curl -sSL https://gitlab.com/KuroNekoTNG/FluffyBot/snippets/1917991/raw -o - | bash'
```

To enable the bot to start at boot, download the [service file][2]. Or you can do:

```
sudo -s -- 'curl -sSL https://gitlab.com/KuroNekoTNG/FluffyBot/snippets/1840076/raw -o /lib/systemd/system/fluffybot.service'
```

And again, if that does not work, use:

```
sudo -- sh -c 'curl -sSL https://gitlab.com/KuroNekoTNG/FluffyBot/snippets/1840076/raw -o /lib/systemd/system/fluffybot.service'
```

Finally, once you have the service file in place, make sure you reload the daemons by doing `systemctl daemon-reload`.
Then do `systemctl enable fluffybot.service`. If you wish to start it, do `systemctl start fluffybot.service`.
I would, however, recommend restarting your system to ensure everything is cooperating nicely.

# After Installing
Once you are done installing the bot, make sure you head over to the Discord Developer area and register your bot.
You will need to grab the token under `Bot` and copy it to the token file (Windows: `C:\ProgramData\FluffyBot\Settings\token` Linux: `/etc/fluffybot/token`.)
Next is to head on over to https://discordapp.com/api/oauth2/authorize?client_id=[INSERT BOT ID HERE]&scope=bot&permissions=[INSER PERMISSION VALUE HERE].
Where \[INSERT BOT ID HERE\] is the Client ID from the General Information section, and \[INSERT PERMISSION VALUE HERE\] is from the permission calculator from the Bot section.
It is recommended NOT to enable admin privleges for the bot, as any security vulnerability means that someone could take over your server through the bot.

Also, it is recommended to install the base module. The base module should be included when you compile/download this software.
To install it, do as stated in the [Module section](#what-can-this-bot-do).

# Issues
If you have any issues regrading my bot, please do not be afraid to open up an issue. Please make sure that you are descriptive as possible and that you be polite.
The following is great for how an issue should be laid out:
```
I have expeienced a bug with the bot where it refuses to accept any calls to it.
Below is the log file that to bot as produced.
```

# Notice
I am not responsible for any damage done to your Discord server through my bot. My bot is provided as is and as such it is your responibility to check all modules that you insert into the bot.

# Technologies used within the bot
This is the list of technologies used within the bot.
*  .NET Core for Cross-platform support.
*  Entity Framework Core to talk to the database.
*  Microsoft.Data.Sqlite as the database backend.
*  Discord.NET to communicate with Discord.

---

[0]: http://discord.gg/SMRaTue
[1]: https://discordapp.com/developers/applications/
[2]: https://gitlab.com/KuroNekoTNG/FluffyBot/snippets/1840076
[3]: https://gitlab.com/KuroNekoTNG/FluffyBot/snippets/1917991
[4]: https://docs.microsoft.com/en-us/dotnet/core/install/linux-package-manager-ubuntu-1904